import _ from 'lodash';
import FuseUtils from '@fuse/utils';

function NotificationModel(data) {
  const payload = data || {};

  return _.defaults(payload, {
    id: FuseUtils.generateGUID(),
    message: '',
    options: {
      variant: 'default',
    },
  });
}

export default NotificationModel;
