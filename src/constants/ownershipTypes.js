const OWNERSHIP_TYPES = {
  SINGLE: 'single',
  JOINT_OWNERSHIP: 'joint ownership',
  ENTITY: 'entity',
};

export default OWNERSHIP_TYPES;
