const WORK_POSITION_TYPES = {
  SELF_EMPLOYED: 'self-employed',
  EMPLOYED: 'employed',
  UNEMPLOYED: 'unemployed',
  RETIRED: 'retired',
  HOMEMAKER: 'homemaker',
  STUDENT: 'student',
};

export default WORK_POSITION_TYPES;
