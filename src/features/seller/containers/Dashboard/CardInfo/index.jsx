import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';

function CardInfo() {
  return (
    <Grid container spacing={2}>
      <Grid container item>
        <Card sx={{ width: '100%' }}>
          <CardContent>
            <Grid container wrap="nowrap" spacing={2}>
              <Grid container item>
                <Typography gutterBottom variant="h5" component="div">
                  0
                </Typography>
              </Grid>
              <Grid container item>
                <Typography variant="body2" color="text.secondary">
                  Number of Listings
                </Typography>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
      <Grid container item>
        <Card sx={{ width: '100%' }}>
          <CardContent>
            <Grid container wrap="nowrap" spacing={2}>
              <Grid container item>
                <Typography gutterBottom variant="h5" component="div">
                  0
                </Typography>
              </Grid>
              <Grid container item>
                <Typography variant="body2" color="text.secondary">
                  Active Contracts
                </Typography>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}

CardInfo.displayName = 'CardInfo';

export default CardInfo;
