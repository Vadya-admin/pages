import { useSelector } from 'react-redux';
import { motion } from 'framer-motion';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

import { selectCurrentUser } from 'features/common/slices/auth';
import CardInfo from './CardInfo';
import CardProfile from './CardProfile';
import CurrentBidsTable from './CurrentBidsTable';
import ActiveContractsTable from './ActiveContractsTable';

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const container = {
  show: {
    transition: {
      staggerChildren: 0.05,
    },
  },
};

const item = {
  hidden: { opacity: 0, y: 40 },
  show: { opacity: 1, y: 0 },
};

function DashboardContainer() {
  const user = useSelector(selectCurrentUser);
  return (
    <Box
      component={motion.div}
      variants={container}
      sx={{ width: '100%', display: 'flex', flex: '1 auto' }}
    >
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={6}>
          <Box component={motion.div} variants={item}>
            <CardProfile profileName={user?.first_name} />
          </Box>
        </Grid>
        <Grid item xs={6}>
          <CardInfo />
        </Grid>
        <Grid item xs={6}>
          <CurrentBidsTable />
        </Grid>
        <Grid item xs={6}>
          <Item>
            <ActiveContractsTable />
          </Item>
        </Grid>
      </Grid>
    </Box>
  );
}

DashboardContainer.displayName = 'DashboardContainer';

export default DashboardContainer;
