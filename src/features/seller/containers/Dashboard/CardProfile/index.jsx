import PropTypes from 'prop-types';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

import { styled } from '@mui/material/styles';
import { Avatar } from '@mui/material';

const HeaderUser = styled(Box)(({ theme }) => ({
  backgroundColor: '#0c1647',
  color: theme.palette.getContrastText('#0c1647'),
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  position: 'relative',
  height: '95px',
}));

function CardProfile({ profileName, role }) {
  return (
    <Card>
      <HeaderUser>
        <Typography gutterBottom variant="h4" component="div">
          Welcome back
        </Typography>
        <Avatar
          sizes="large"
          sx={{
            position: 'absolute',
            left: '12px',
            bottom: '-18px',
            width: '72px',
            height: '72px',
          }}
        />
      </HeaderUser>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {profileName}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {role}
        </Typography>
      </CardContent>
    </Card>
  );
}

CardProfile.displayName = 'CardProfile';

CardProfile.propTypes = {
  profileName: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired,
};

export default CardProfile;
