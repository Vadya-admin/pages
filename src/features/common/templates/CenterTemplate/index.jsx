import PropTypes from 'prop-types';

function CenterTemplate({ children }) {
  return <div className="flex w-full h-hull justify-center items-center">{children}</div>;
}

CenterTemplate.displayName = 'CenterTemplate';

CenterTemplate.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CenterTemplate;
