import axios from 'shared/lib/axios';
import { getToken } from 'shared/lib/localStorage/index';

export const axiosInstance = axios.create({
  transformRequest: [function (data, headers) {
      if (headers['Content-Type'] && headers['Content-Type'].startsWith('multipart/form-data')) {
          const form = new FormData();
          for (const key in data) {
              const value = data[key];
              if (Array.isArray(value)) {
                  const arrayKey = `${key}[]`;
                  value.forEach(v => {
                      form.append(arrayKey, v);
                  });
              } else{
                  form.append(key, value);
              }
          }
          return form;
      }
      return data;
  }, ...axios.defaults.transformRequest],
});

// auth functionality
export const loginRequest = async ({ email, password }) => {
  const response = await axiosInstance.post('/auth/login', { email, password });
  return response.data;
};

export const registrationRequest = async (data) => {
  const response = await axiosInstance.post('/auth/registration', data);
  return response.data;
};

export const forgotPasswordRequest = async data => {
  const response = await axiosInstance.post('/password/forgot', data);
  return response.data;
}

export const resetPasswordRequest = async data => {
  const response = await axiosInstance.post('/password/reset', data);
  return response.data;
}

export const userPreferencesRequest = async ({ token }) => {
  const response = await axiosInstance.get('/user', {
    headers: { Authorization: `Bearer ${token}` },
  });
  return response.data;
};

// data
export const getTermsRequest = async () => {
  const response = await axiosInstance.get('/terms');
  return response.data;
};

// listing
export const createListingRequest = async data => {
  const response = await axiosInstance.post('/listing', data, {
    headers: { Authorization: `Bearer ${getToken()}` },
  });
  return response.data;
};

export const updateListingRequest = async (listingId, data) => {
  const response = await axiosInstance.put(`/listing/${listingId}`, data, {
    headers: { Authorization: `Bearer ${getToken()}` },
  });
  return response.data;
};

export const getListings = async (params) => {
  const response = await axiosInstance.get('/listing', {
    headers: { Authorization: `Bearer ${getToken()}` },
    params
  });
  return response.data;
};

export const getListingById = async listingId => {
  const response = await axiosInstance.get(`/listing/${listingId}`, {
    headers: { Authorization: `Bearer ${getToken()}` }
  });
  return response.data;
};

export const deleteListingById = async listingId => {
  const response = await axiosInstance.delete(`/listing/${listingId}`, {
    headers: { Authorization: `Bearer ${getToken()}` }
  });
  return response.data;
};

// contract
export const getContractPreview = async data => {
  const response = await axiosInstance.get('/listing/contract/preview', {
    headers: { Authorization: `Bearer ${getToken()}` },
    params: data
  });
  return response.data;
}

export const getContractByListingId = async listingId => {
  const response = await axiosInstance.get(`/listing/${listingId}/contract`, {
    headers: { Authorization: `Bearer ${getToken()}` },
  });
  return response.data;
}