import { createSlice } from '@reduxjs/toolkit';

const slice = createSlice({
  name: 'api',
  initialState: {
    isLoading: false,
  },
  reducers: {
    requestStart: (state) => {
      state.isLoading = true;
    },
    requestEnd: (state) => {
      state.isLoading = false;
    },
  },
});

export const { requestStart, requestEnd } = slice.actions;

export default slice.reducer;

export const checkIfIsLoading = (state) => state.api.isLoading;
