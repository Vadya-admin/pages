import { createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import * as auth from 'features/common/api';
import { requestEndAction, requestStartAction } from '../actions/api';

export const authThunk = createAsyncThunk(
  'auth/authThunk',
  async ({ email, password }, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const data = await auth.loginRequest({ email, password });
      dispatch(requestEndAction());
      return data;
    } catch (error) {
      dispatch(requestEndAction());
      if (error.response?.status === 422) {
        toast.error('Invalid login or password');
      } else {
        toast.error(`Error ${error.message}`);
      }
      return rejectWithValue(error);
    }
  }
);

export const forgotPasswordThunk = createAsyncThunk(
  'auth/forgotPasswordThunk',
  async (payload, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const data = await (payload.token ? auth.resetPasswordRequest(payload) : auth.forgotPasswordRequest(payload));
      dispatch(requestEndAction());
      toast.success(data.data[0]);
      return data;
    } catch (error) {
      dispatch(requestEndAction());
      toast.error(Object.values(error.response.data)[0][0]);
      return rejectWithValue(error);
    }
  }
);

export const registrationThunk = createAsyncThunk(
  'auth/registrationThunk',
  async (payload, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const data = await auth.registrationRequest(payload);
      dispatch(requestEndAction());
      return data;
    } catch (error) {
      dispatch(requestEndAction());
      const errorsText = [];
      Object.values(error.response.data).forEach((errors) => {
        errors.forEach((errorText) => {
          errorsText.push(errorText);
        });
      });
      toast.error(errorsText.join(', '));
      return rejectWithValue(error);
    }
  }
);

export const termsThunk = createAsyncThunk(
  'auth/termsThunk',
  async (_, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;
    try {
      dispatch(requestStartAction());
      const data = await auth.getTermsRequest();
      dispatch(requestEndAction());
      return data;
    } catch (error) {
      dispatch(requestEndAction());
      const errorsText = [];
      Object.values(error.response.data).forEach((errors) => {
        errors.forEach((errorText) => {
          errorsText.push(errorText);
        });
      });
      toast.error(errorsText.join(', '));
      return rejectWithValue(error);
    }
  }
);

export const userPreferencesThunk = createAsyncThunk(
  'auth/userPreferencesThunk',
  async (localToken, thunkApi) => {
    const { rejectWithValue } = thunkApi;

    try {
      const data = await auth.userPreferencesRequest({ token: localToken });
      return { ...data, token: localToken };
    } catch (error) {
      toast.error(`Error ${error.message}`);
      return rejectWithValue(error);
    }
  }
);
