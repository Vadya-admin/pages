import { useState, useEffect } from 'react';

export const DRAWER_NAME = 'alphamize_drawer';

export const getDrawer = () => localStorage.getItem(DRAWER_NAME);

export const setDrawer = (state) => localStorage.setItem(DRAWER_NAME, state);

export const withOpenDrawer = (Component) =>
  function (props) {
    const [open, _] = useState(true);

    useEffect(() => {
      const drawer = getDrawer();

      if (drawer) {
        // setOpen(drawer.toLowerCase() === 'true');
      }
    }, []);

    const handleDrawerOpen = () => {
      // setOpen(true);
      // setDrawer(true);
    };

    const handleDrawerClose = () => {
      // setOpen(false);
      // setDrawer(false);
    };

    return (
      <Component
        {...props}
        open={open}
        onOpenDrawer={handleDrawerOpen}
        onCloseDrawer={handleDrawerClose}
      />
    );
  };
