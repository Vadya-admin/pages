import { motion } from 'framer-motion';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';

import SimpleChart from './simple-chart';

const container = {
  show: {
    transition: {
      staggerChildren: 0.05,
    },
  },
};

const item = {
  hidden: { opacity: 0, y: 40 },
  show: { opacity: 1, y: 0 },
};

function Charts() {
  return (
    <Box component={motion.div} variants={container} sx={{ width: '100%', display: 'flex' }}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <Card
            component={motion.div}
            sx={{ width: '100%', height: '300px' }}
            variants={item}
            className="w-full mb-32 p-20 rounded-16 shadow"
          >
            <SimpleChart />
          </Card>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Card
            component={motion.div}
            variants={item}
            sx={{ width: '100%', height: '300px' }}
            className="w-full mb-32 p-20 rounded-16 shadow"
          >
            <SimpleChart />
          </Card>
        </Grid>
      </Grid>
    </Box>
  );
}

export default Charts;
