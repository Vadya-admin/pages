import axios from 'shared/lib/axios';

export const uploadAvatarRequest = async ({ token, file }) => {
  const response = await axios.post('/user/avatar', file, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return response.data;
};

export const deleteAvatarRequest = async ({ token }) => {
  const response = await axios.delete('/user/avatar', {
    headers: { Authorization: `Bearer ${token}` },
  });
  return response.data;
};

export const updateSellerProfileRequest = async ({ token, profile, id }) => {
  const response = await axios.put(`/user/${id}/profile`, profile, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return response.data;
};

export const updateBuyerProfileRequest = async ({ token, profile, id }) => {
  const response = await axios.put(`/user/${id}/profile`, profile, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return response.data;
};

export const getTitlesRequest = async () => {
  const response = await axios.get('/titles', {
    // headers: { Authorization: `Bearer ${token}` },
  });
  return response.data;
};

export const getStates = async () => {
  const response = await axios.get('/states');
  return response.data;
};

export const getCities = async (stateId) => {
  const response = await axios.get(`/states/${stateId}/cities`);
  return response.data;
};

export const updateProfileRequest = async ({ token, profile, id }) => {
  const response = await axios.put(`/user/${id}`, profile, {
    headers: { Authorization: `Bearer ${token}` },
  });
  return response.data;
};
