import { Typography } from '@mui/material';
import { Helmet } from 'react-helmet';

import InfoProfile from 'features/profile/containers/Info';

/* eslint-disable import/prefer-default-export */
export function ProfilePage() {
  return (
    <>
      <Helmet>
        <title>Alphamize | Profile</title>
      </Helmet>
      <Typography variant="h5" gutterBottom>
        Profile
      </Typography>
      <InfoProfile />
    </>
  );
}

ProfilePage.displayName = 'ProfilePage';
