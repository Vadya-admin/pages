import { useState } from 'react';
import classNames from 'classnames/bind';
import { Helmet } from 'react-helmet';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import JoinTemplate from 'features/common/templates/JoinTemplate';
import styles from './styles.module.css';
import ResetPasswordForm from './components/ResetPasswordForm';
import { useDispatch } from 'react-redux';
import { forgotPasswordThunk } from 'features/common/thunks/auth';
import { useNavigate } from 'react-router-dom';
import paths from 'app/Routes/paths';

const cx = classNames.bind(styles);

/* eslint-disable import/prefer-default-export */
export function ResetPasswordPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [resetted, setResetted] = useState(false);

  const handleSubmit = async (values) => {
    const data = values.token ? values : { email: values.email }
    const response = await dispatch(forgotPasswordThunk(data));
    if (values.token && response.payload.success) {
        setResetted(true);
    }
  };

  return (
    <>
      <Helmet>
        <title>Reset Password</title>
      </Helmet>
      <JoinTemplate>
        <section className="flex justify-center items-center">
          <div>
            {!resetted && <Button type="button" onClick={() => navigate(paths.signIn())}>
              {`Back To Login`}
            </Button>}
            <Typography variant="h2" gutterBottom>
              Reset Password
            </Typography>
            <Typography variant="subtitle1" className="py-12" gutterBottom component="div">
              {resetted ? 'Password is changed successfully' : 'Enter your email address that you entered on your registration form and a new password'}
            </Typography>
            <div className={cx('form')}>
              {resetted ? <Button type="button" onClick={() => navigate(paths.signIn())}>
            Back to Login
          </Button>  : <ResetPasswordForm onSubmit={handleSubmit} />}
            </div>
          </div>
        </section>
      </JoinTemplate>
    </>
  );
}

ResetPasswordPage.displayName = 'ResetPasswordPage';
