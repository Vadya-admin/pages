import { Button, Card, CardContent, Grid, Typography } from '@mui/material';

function ListingVerification({ buttons }) {
  return (
    <Grid
      container
      columns={12}
      columnSpacing={2}
      rowSpacing={2}
      justifyContent="center"
      align="center"
    >
      <Grid item xs={8}>
        <Card>
          <CardContent>
            <Typography variant="body2">
              Your listing is under review. You will receive the email notification after approving.
            </Typography>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12}>
        {buttons &&
          buttons.map((button) => (
            <Button onClick={button.onClick} type={button.type} variant="secondary">
              {button.text}
            </Button>
          ))}
      </Grid>
    </Grid>
  );
}

ListingVerification.displayName = 'ListingVerification';

export default ListingVerification;
