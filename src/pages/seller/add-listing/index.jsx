import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const AddListingPage = loadable(() => import(/* webpackChunkName: "seller-page" */ './page'), {
  resolveComponent: ({ AddListingPage: Page }) => Page,
  fallback: <Loading />,
});

export default AddListingPage;
