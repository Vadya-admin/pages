import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const SellerHomePage = loadable(() => import(/* webpackChunkName: "seller-page" */ './page'), {
  resolveComponent: ({ SellerHomePage: Page }) => Page,
  fallback: <Loading />,
});

export default SellerHomePage;
