import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import paths from 'app/Routes/paths';
import classNames from 'classnames/bind';
import JoinTemplate from 'features/common/templates/JoinTemplate';
import { registrationThunk } from 'features/common/thunks/auth';
import { useFormik } from 'formik';
import { useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import ACCOUNT_TYPES from 'src/constants/accountTypes';
import ADDRESS_TYPES from 'src/constants/addressTypes';
import OWNERSHIP_TYPES from 'src/constants/ownershipTypes';
import PHONE_TYPES from 'src/constants/phoneTypes';
import WORK_POSITION_TYPES from 'src/constants/workPositionTypes';
import * as Yup from 'yup';
import ROLES from '../../../features/common/enums';
import SignUpForm from './components/SignUpForm';
import SignUpWizard from './components/SignUpWizard';
import AccountInformation from './components/SignUpWizard/steps/AccountInformation';
import BankInformation from './components/SignUpWizard/steps/BankInformation';
import Disclosures from './components/SignUpWizard/steps/Disclosures';
import PersonalInformation from './components/SignUpWizard/steps/PersonalInformation/index';
import styles from './styles.module.css';

const cx = classNames.bind(styles);

const validationSchema = (currentStep, role) =>
  role === ROLES.Buyer
    ? currentStep === 0
      ? Yup.object({
          user: Yup.object().shape({
            firstName: Yup.string('Enter your first name').required('First name is required'),
            lastName: Yup.string('Enter your last name').required('Last name is required'),
            email: Yup.string('Enter your email')
              .email('Enter a valid email')
              .required('Email is required'),
            password: Yup.string('Enter your password')
              .min(8, 'Password should be of minimum 8 characters length')
              .required('Password is required'),
            passwordConfirmation: Yup.string('Enter your password')
              .min(8)
              .oneOf([Yup.ref('password'), null], 'Passwords must match')
              .required('Required')
              .trim(),
          }),
        })
      : currentStep === 1
      ? Yup.object({
          profile: Yup.object().shape({
            company: Yup.string('Enter company name').required('Company name is required'),
            title: Yup.string('Enter title').required('Title is required'),
          }),
          homePhone: Yup.object().shape({
            number: Yup.string('Enter phone number').required('Phone is required'),
          }),
          homeAddress: Yup.object().shape({
            lineOne: Yup.string('Enter Address Line 1')
              .required('Address Line 1 is required')
              .min(3, 'Must be at least 3 characters'),
            lineTwo: Yup.string('Enter Address Line 2').min(3, 'Must be at least 3 characters'),
            city: Yup.string('Enter city').required('City is required'),
            state: Yup.string('Enter state').required('State is required'),
            zipCode: Yup.string('Enter zip code').required('Zip code is required'),
          }),
        })
      : currentStep === 2
      ? Yup.object({
          termsAgreed: Yup.bool().oneOf([true], 'Please accept Terms and Agreements'),
          privacyPolicyAgreed: Yup.bool().oneOf([true], 'Please accept Privacy Policy'),
          serviceAgreed: Yup.bool().oneOf([true], 'Please accept Terms and Service'),
        })
      : Yup.object({
          bankInformation: Yup.object().shape({
            accountType: Yup.string('Choose account type')
              .required('Account Type is required')
              .oneOf(Object.values(ACCOUNT_TYPES), 'Incorrect account type'),
            holderName: Yup.string('Enter holder name').required('Holder name is required'),
            accountNumber: Yup.string('Enter account number').required(
              'Account number is required'
            ),
            confirmAccountNumber: Yup.string('Confirm account number')
              .required('Confirmation of account number is required')
              .oneOf([Yup.ref('accountNumber'), null], 'account numbers must match'),
            routingNumber: Yup.string('Choose routing number').required(
              'Routing number is required'
            ),
          }),
        })
    : currentStep === 0
    ? Yup.object({
        user: Yup.object().shape({
          firstName: Yup.string('Enter your first name').required('First name is required'),
          lastName: Yup.string('Enter your last name').required('Last name is required'),
          email: Yup.string('Enter your email')
            .email('Enter a valid email')
            .required('Email is required'),
          password: Yup.string('Enter your password')
            .min(8, 'Password should be of minimum 8 characters length')
            .required('Password is required'),
          passwordConfirmation: Yup.string('Enter your password')
            .min(8)
            .oneOf([Yup.ref('password'), null], 'Passwords must match')
            .required('Required')
            .trim(),
        }),
      })
    : currentStep === 1
    ? Yup.object({
        termsAgreed: Yup.bool().oneOf([true], 'Please accept Terms and Agreements'),
        privacyPolicyAgreed: Yup.bool().oneOf([true], 'Please accept Privacy Policy'),
        serviceAgreed: Yup.bool().oneOf([true], 'Please accept Terms and Service'),
      })
    : currentStep === 2
    ? Yup.object({
        sellerProfile: Yup.object().shape({
          middleName: Yup.string('Enter middle name').required('Middle name is required').min(3, 'Must be at least 3 characters'),
          estateName: Yup.string('Enter estate name').required('Estate name is required'),
          socialSecurityNumber: Yup.string('Enter social security number').required(
            'Social security number is required'
          ),
          birthDate: Yup.string('Enter date of birth').required('Date of birth is required').typeError('Wrong date format'),
          title: Yup.string('Enter title').required('Title is required'),
          employerName: Yup.string('Enter employer name').required('Employer name is required'),
          work: Yup.string('Choose your work position')
            .required('Work position is required')
            .oneOf(Object.values(WORK_POSITION_TYPES), 'Wrong work position'),
        }),
        primaryPhone: Yup.object().shape({
          number: Yup.string('Enter primary phone number').required('Primary phone is required'),
        }),
        secondaryPhone: Yup.object().shape({
          number: Yup.string('Enter secondary phone number').required(
            'Secondary phone is required'
          ),
        }),
        homeAddress: Yup.object().shape({
          lineOne: Yup.string('Enter Address Line 1')
            .required('Address Line 1 is required')
            .min(3, 'Must be at least 3 characters'),
          lineTwo: Yup.string('Enter Address Line 2').min(3, 'Must be at least 3 characters'),
          city: Yup.string('Enter city').required('City is required'),
          state: Yup.string('Enter state').required('State is required'),
          zipCode: Yup.string('Enter zip code').required('Zip code is required'),
        }),
        mailingAddress: Yup.object().shape({
          lineOne: Yup.string('Enter Address Line 1')
            .required('Address Line 1 is required')
            .min(3, 'Must be at least 3 characters'),
          lineTwo: Yup.string('Enter Address Line 2').min(3, 'Must be at least 3 characters'),
          city: Yup.string('Enter city').required('City is required'),
          state: Yup.string('Enter state').required('State is required'),
          zipCode: Yup.string('Enter zip code').required('Zip code is required'),
        }),
        employerAddress: Yup.object().shape({
          lineOne: Yup.string('Enter Address Line 1')
            .required('Address Line 1 is required')
            .min(3, 'Must be at least 3 characters'),
          lineTwo: Yup.string('Enter Address Line 2').min(3, 'Must be at least 3 characters'),
          city: Yup.string('Enter city').required('City is required'),
          state: Yup.string('Enter state').required('State is required'),
          zipCode: Yup.string('Enter zip code').required('Zip code is required'),
        }),
      })
    : Yup.object({
        bankInformation: Yup.object().shape({
          accountType: Yup.string('Choose account type')
            .required('Account Type is required')
            .oneOf(Object.values(ACCOUNT_TYPES), 'Wrong account type'),
          holderName: Yup.string('Enter holder name').required('Holder name is required'),
          accountNumber: Yup.string('Enter account number').required('Account number is required'),
          confirmAccountNumber: Yup.string('Confirm account number')
            .required('Confirmation of account number is required')
            .oneOf([Yup.ref('accountNumber'), null], 'account numbers must match'),
          routingNumber: Yup.string('Choose routing number').required('Routing number is required'),
        }),
      });

/* eslint-disable import/prefer-default-export */
export function SignUpPage() {
  const [currentStep, setCurrentStep] = useState(0);
  const [registrationModalContent, setRegistrationModalContent] = useState(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onSubmit = async (values) => {
    if (currentStep !== steps.length) {
      setCurrentStep(currentStep + 1);
    } else {
      if (!values.homeAddress.lineTwo) delete values.homeAddress.lineTwo;
      if (!values.mailingAddress.lineTwo) delete values.mailingAddress.lineTwo;
      if (!values.employerAddress.lineTwo) delete values.employerAddress.lineTwo;
      const data =
        values.user.role === ROLES.Buyer
          ? {
              user: values.user,
              profile: values.profile,
              addresses: [{ ...values.homeAddress, type: ADDRESS_TYPES.HOME }],
              phones: [{ ...values.homePhone, type: PHONE_TYPES.BASE }],
              bankInformation: values.bankInformation,
            }
          : {
              user: values.user,
              profile: values.sellerProfile,
              addresses: [
                { ...values.homeAddress, type: ADDRESS_TYPES.HOME },
                { ...values.employerAddress, type: ADDRESS_TYPES.EMPLOYER },
                { ...values.mailingAddress, type: ADDRESS_TYPES.MAILING },
              ],
              phones: [
                { ...values.primaryPhone, type: PHONE_TYPES.PRIMARY },
                { ...values.secondaryPhone, type: PHONE_TYPES.SECONDARY },
              ],
              bankInformation: values.bankInformation,
            };
      const response = await dispatch(registrationThunk(data));
      if (response?.payload?.success) {
        setRegistrationModalContent(
          'Your Account is now being verified. Please allow for 24-48 Hours. You will be emailed once your account is approved.'
        );
      }
    }
  };

  const formik = useFormik({
    initialValues: {
      user: {
        firstName: '',
        lastName: '',
        passwordConfirmation: '',
        email: '',
        password: '',
        role: ROLES.Buyer,
      },
      profile: {
        title: '',
        company: '',
      },
      sellerProfile: {
        middleName: '',
        title: '',
        estateName: '',
        socialSecurityNumber: '',
        birthDate: new Date(),
        employerName: '',
        ownership: OWNERSHIP_TYPES.SINGLE,
        listingOwnership: OWNERSHIP_TYPES.SINGLE,
        work: WORK_POSITION_TYPES.SELF_EMPLOYED,
      },
      homeAddress: {
        lineOne: '',
        lineTwo: '',
        city: '',
        state: '',
        zipCode: '',
      },
      mailingAddress: {
        lineOne: '',
        lineTwo: '',
        city: '',
        state: '',
        zipCode: '',
      },
      employerAddress: {
        lineOne: '',
        lineTwo: '',
        city: '',
        state: '',
        zipCode: '',
      },
      homePhone: {
        number: '',
      },
      primaryPhone: {
        number: '',
      },
      secondaryPhone: {
        number: '',
      },
      termsAgreed: false,
      privacyPolicyAgreed: false,
      serviceAgreed: false,
      bankInformation: {
        accountType: ACCOUNT_TYPES.SAVING,
        holderName: '',
        accountNumber: '',
        confirmAccountNumber: '',
        routingNumber: '',
      },
    },
    validationSchema: () => Yup.lazy((values) => validationSchema(currentStep, values.user.role)),
    validateOnMount: false,
    validateOnChange: false,
    onSubmit,
  });

  const steps =
    formik.values.user.role === ROLES.Buyer
      ? [
          {
            label: 'Account information',
            pageComponent: () => <AccountInformation form={formik} />,
          },
          {
            label: 'Disclosures',
            pageComponent: () => <Disclosures form={formik} />,
          },
          {
            label: 'Bank information',
            pageComponent: () => <BankInformation form={formik} />,
          },
        ]
      : [
          {
            label: 'Disclosures',
            pageComponent: () => <Disclosures form={formik} />,
          },
          {
            label: 'Personal Information',
            pageComponent: () => <PersonalInformation form={formik} />,
          },
          {
            label: 'Bank information',
            pageComponent: () => <BankInformation form={formik} />,
          },
        ];

  return (
    <>
      <Helmet>
        <title>Alphamize | Sign in</title>
      </Helmet>
      <Dialog
        onClose={() => {
          setRegistrationModalContent(null);
          navigate(paths.signIn());
        }}
        open={!!registrationModalContent}
      >
        <DialogContent>
          <Typography variant="body2">{registrationModalContent}</Typography>
          <Button type="button" onClick={() => navigate(paths.signIn())}>
            Back to Login
          </Button>
        </DialogContent>
      </Dialog>
      {currentStep === 0 ? (
        <JoinTemplate>
          <section className="flex justify-center items-center">
            <div className={cx('container')}>
              <div className="flex justify-start">
                <Typography variant="h2" gutterBottom>
                  Register
                </Typography>
              </div>
              <div className={cx('form')}>
                <SignUpForm form={formik} />
                <Divider />
                <footer className="flex items-center py-12">
                  <div>
                    Already have an account?&nbsp;
                    <Link to={paths.signIn()}>Sign in</Link>
                  </div>
                </footer>
              </div>
            </div>
          </section>
        </JoinTemplate>
      ) : (
        <SignUpWizard
          steps={steps}
          currentStep={currentStep - 1}
          handleSubmit={formik.handleSubmit}
          handleBack={() => setCurrentStep(currentStep - 1)}
          handleNext={formik.handleSubmit}
        />
      )}
    </>
  );
}

SignUpPage.displayName = 'SignUpPage';
