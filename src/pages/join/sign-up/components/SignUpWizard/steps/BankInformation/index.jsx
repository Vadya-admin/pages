import PropTypes from 'prop-types';
import { TextField, Grid } from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import capitalize from 'lodash/capitalize';
import Select from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import ACCOUNT_TYPES from 'src/constants/accountTypes';
import InputLabel from '@mui/material/InputLabel';

export default function BankInformation({ form }) {
  return (
    <form onSubmit={form.handleSubmit}>
      <Grid container columns={12} columnSpacing={2} rowSpacing={4}>
        <Grid item xs={12}>
          <FormControl fullWidth>
            <InputLabel>Account Type</InputLabel>
            <Select
              name="bankInformation.accountType"
              label="Account Type"
              error={
                form.errors.bankInformation?.accountType &&
                Boolean(form.errors.bankInformation?.accountType)
              }
              onChange={form.handleChange}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.bankInformation?.accountType)}
            >
              {Object.values(ACCOUNT_TYPES).map((title) => (
                <MenuItem key={title} value={title}>
                  {capitalize(title)}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="bankInformation.holderName"
            label="Account Holder Name"
            fullWidth
            error={
              form.errors.bankInformation?.holderName &&
              Boolean(form.errors.bankInformation?.holderName)
            }
            helperText={
              form.touched.bankInformation?.holderName && form.errors.bankInformation?.holderName
            }
            onChange={form.handleChange}
            value={form.values.bankInformation.holderName}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="bankInformation.accountNumber"
            label="Account Number"
            fullWidth
            error={
              form.errors.bankInformation?.accountNumber &&
              Boolean(form.errors.bankInformation?.accountNumber)
            }
            helperText={
              form.touched.bankInformation?.accountNumber &&
              form.errors.bankInformation?.accountNumber
            }
            onChange={form.handleChange}
            value={form.values.bankInformation.accountNumber}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="bankInformation.confirmAccountNumber"
            label="Confirm Account Number"
            fullWidth
            error={
              form.errors.bankInformation?.confirmAccountNumber &&
              Boolean(form.errors.bankInformation?.confirmAccountNumber)
            }
            helperText={
              form.touched.bankInformation?.confirmAccountNumber &&
              form.errors.bankInformation?.confirmAccountNumber
            }
            onChange={form.handleChange}
            value={form.values.bankInformation.confirmAccountNumber}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="bankInformation.routingNumber"
            label="Routing Number"
            fullWidth
            error={
              form.errors.bankInformation?.routingNumber &&
              Boolean(form.errors.bankInformation?.routingNumber)
            }
            helperText={
              form.touched.bankInformation?.routingNumber &&
              form.errors.bankInformation?.routingNumber
            }
            onChange={form.handleChange}
            value={form.values.bankInformation.routingNumber}
          />
        </Grid>
      </Grid>
    </form>
  );
}

BankInformation.displayName = 'BankInformation';

BankInformation.propTypes = {
  form: PropTypes.shape({
    errors: PropTypes.shape({
      bankInformation: PropTypes.shape({
        accountNumber: PropTypes.string,
        accountType: PropTypes.string,
        confirmAccountNumber: PropTypes.string,
        holderName: PropTypes.string,
        routingNumber: PropTypes.string,
      }),
    }),
    handleChange: PropTypes.func,
    handleSubmit: PropTypes.func,
    touched: PropTypes.shape({
      bankInformation: PropTypes.shape({
        accountNumber: PropTypes.bool,
        confirmAccountNumber: PropTypes.bool,
        holderName: PropTypes.bool,
        routingNumber: PropTypes.bool,
      }),
    }),
    values: PropTypes.shape({
      bankInformation: PropTypes.shape({
        accountNumber: PropTypes.string,
        accountType: PropTypes.string,
        confirmAccountNumber: PropTypes.string,
        holderName: PropTypes.string,
        routingNumber: PropTypes.string,
      }),
    }),
  }).isRequired,
};
