import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import {
  TextField,
  FormControl,
  FormLabel,
  InputLabel,
  Radio,
  RadioGroup,
  Select,
  MenuItem,
  FormHelperText,
  Typography,
} from '@mui/material';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import capitalize from 'lodash/capitalize';
import { getCities, getStates, getTitlesRequest } from 'features/profile/api/index';
import WORK_POSITION_TYPES from 'src/constants/workPositionTypes';
import OWNERSHIP_TYPES from 'src/constants/ownershipTypes';

function PersonalInformation({ form }) {
  const [states, setStates] = useState(null);
  const [cities, setCities] = useState(null);
  const [titles, setTitles] = useState(null);

  useEffect(async () => {
    const { data: statesData } = await getStates();
    setStates(statesData.states);

    const { data: titlesData } = await getTitlesRequest();
    setTitles(titlesData);
  }, []);

  const handleOpenCitySelect = async (field) => {
    const stateId = states.find((state) => state.name === form.values[field].state).id;
    const { data } = await getCities(stateId);
    setCities(data.cities);
  };

  return (
    <form onSubmit={form.handleSubmit}>
      <Grid container columns={12} columnSpacing={2} rowSpacing={4}>
        <Grid item xs={12}>
          <FormControl component="fieldset">
            <FormLabel component="legend">Ownership</FormLabel>
            <RadioGroup
              row
              value={form.values.sellerProfile.ownership}
              onChange={(e) => form.setFieldValue('sellerProfile.ownership', e.currentTarget.value)}
              name="form.sellerProfile.ownership"
            >
              {Object.values(OWNERSHIP_TYPES).map((type) => (
                <FormControlLabel
                  key={type}
                  value={type}
                  control={<Radio />}
                  label={capitalize(type)}
                />
              ))}
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControl component="fieldset">
            <FormLabel component="legend">Listing Ownership</FormLabel>
            <RadioGroup
              row
              value={form.values.sellerProfile.listingOwnership}
              onChange={(e) =>
                form.setFieldValue('sellerProfile.listingOwnership', e.currentTarget.value)
              }
              name="form.sellerProfile.listingOwnership"
            >
              {Object.values(OWNERSHIP_TYPES).map((type) => (
                <FormControlLabel
                  key={type}
                  value={type}
                  control={<Radio />}
                  label={capitalize(type)}
                />
              ))}
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="user.firstName"
            label="First name"
            fullWidth
            disabled
            error={form.errors.user?.firstName && Boolean(form.errors.user?.firstName)}
            helperText={form.touched.user?.firstName && form.errors.user?.firstName}
            onChange={form.handleChange}
            value={form.values.user.firstName}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="sellerProfile.middleName"
            label="Middle name"
            fullWidth
            error={
              form.errors.sellerProfile?.middleName &&
              Boolean(form.errors.sellerProfile?.middleName)
            }
            helperText={
              form.touched.sellerProfile?.middleName && form.errors.sellerProfile?.middleName
            }
            onChange={form.handleChange}
            value={form.values.sellerProfile?.middleName}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="user.lastName"
            disabled
            label="Last name"
            fullWidth
            error={form.errors.user?.lastName && Boolean(form.errors.user?.lastName)}
            helperText={form.touched.user?.lastName && form.errors.user?.lastName}
            onChange={form.handleChange}
            value={form.values.user?.lastName}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="sellerProfile.estateName"
            label="Estate name"
            fullWidth
            error={
              form.errors.sellerProfile?.estateName &&
              Boolean(form.errors.sellerProfile?.estateName)
            }
            helperText={
              form.touched.sellerProfile?.estateName && form.errors.sellerProfile?.estateName
            }
            onChange={form.handleChange}
            value={form.values.sellerProfile?.estateName}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="sellerProfile.socialSecurityNumber"
            label="Social security number"
            fullWidth
            error={
              form.errors.sellerProfile?.socialSecurityNumber &&
              Boolean(form.errors.sellerProfile?.socialSecurityNumber)
            }
            helperText={
              form.touched.sellerProfile?.socialSecurityNumber &&
              form.errors.sellerProfile?.socialSecurityNumber
            }
            onChange={form.handleChange}
            value={form.values.sellerProfile?.socialSecurityNumber}
          />
        </Grid>
        <Grid item xs={4}>
          <DesktopDatePicker
            name="sellerProfile.birthDate"
            label="Date of birth"
            inputFormat="MM/dd/yyyy"
            renderInput={(params) => <TextField fullWidth {...params}             error={
              form.errors.sellerProfile?.birthDate && Boolean(form.errors.sellerProfile?.birthDate)
            }/>}

            onChange={(value) => form.setFieldValue('sellerProfile.birthDate', value)}
            value={form.values.sellerProfile?.birthDate}
          />
                              <FormHelperText error>
            {form.touched.sellerProfile?.birthDate && form.errors.sellerProfile?.birthDate}
          </FormHelperText>
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="primaryPhone.number"
            label="Primary phone number"
            fullWidth
            error={form.errors.primaryPhone?.number && Boolean(form.errors.primaryPhone?.number)}
            helperText={form.touched.primaryPhone?.number && form.errors.primaryPhone?.number}
            onChange={form.handleChange}
            value={form.values.primaryPhone?.number}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="secondaryPhone.number"
            label="Secondary phone number"
            fullWidth
            error={
              form.errors.secondaryPhone?.number && Boolean(form.errors.secondaryPhone?.number)
            }
            helperText={form.touched.secondaryPhone?.number && form.errors.secondaryPhone?.number}
            onChange={form.handleChange}
            value={form.values.secondaryPhone?.number}
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            name="user.email"
            label="Email"
            disabled
            fullWidth
            error={form.errors.user?.email && Boolean(form.errors.user?.email)}
            helperText={form.touched.user?.email && form.errors.user?.email}
            onChange={form.handleChange}
            value={form.values.user?.email}
          />
        </Grid>
        <Grid item xs={4}>
          <FormControl fullWidth>
            <InputLabel>Title</InputLabel>
            <Select
              name="sellerProfile.title"
              label="Title"
              error={form.errors.sellerProfile?.title && Boolean(form.errors.sellerProfile?.title)}
              onChange={form.handleChange}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.sellerProfile?.title)}
            >
              <MenuItem disabled value="">
                <span>Title</span>
              </MenuItem>
              {titles &&
                titles.map((title) => (
                  <MenuItem key={title} value={title}>
                    {capitalize(title)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.sellerProfile?.title && form.errors.sellerProfile?.title}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="subtitle2">Home address</Typography>
        </Grid>
        <Grid item xs={3}>
          <TextField
            name="homeAddress.lineOne"
            label="Address line 1"
            fullWidth
            error={form.errors.homeAddress?.lineOne && Boolean(form.errors.homeAddress?.lineOne)}
            helperText={form.touched.homeAddress?.lineOne && form.errors.homeAddress?.lineOne}
            onChange={form.handleChange}
            value={form.values.homeAddress.lineOne}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            name="homeAddress.lineTwo"
            label="Address line 2"
            fullWidth
            error={form.errors.homeAddress?.lineTwo && Boolean(form.errors.homeAddress?.lineTwo)}
            helperText={form.touched.homeAddress?.lineTwo && form.errors.homeAddress?.lineTwo}
            onChange={form.handleChange}
            value={form.values.homeAddress.lineTwo}
          />
        </Grid>
        <Grid item xs={3}>
          <FormControl fullWidth>
            <InputLabel>State</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="homeAddress.state"
              label="State"
              error={form.errors.homeAddress?.state && Boolean(form.errors.homeAddress?.state)}
              onChange={(e) => {
                form.setFieldValue('homeAddress.city', '');
                form.handleChange(e);
              }}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.homeAddress?.state)}
            >
              <MenuItem disabled value="">
                <span>State</span>
              </MenuItem>
              {states &&
                states.map((state) => (
                  <MenuItem key={state.id} value={state.name}>
                    {capitalize(state.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.homeAddress?.state && form.errors.homeAddress?.state}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={3}>
          <FormControl fullWidth>
            <InputLabel>City</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="homeAddress.city"
              onOpen={() => handleOpenCitySelect('homeAddress')}
              label="City"
              disabled={!form.values.homeAddress.state}
              error={form.errors.homeAddress?.city && Boolean(form.errors.homeAddress?.city)}
              onChange={form.handleChange}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.homeAddress?.city)}
            >
              <MenuItem disabled value="">
                <span>City</span>
              </MenuItem>
              {cities &&
                cities.map((city) => (
                  <MenuItem key={city.id} value={city.name}>
                    {capitalize(city.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.homeAddress?.city && form.errors.homeAddress?.city}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={3}>
          <TextField
            name="homeAddress.zipCode"
            label="Zip code"
            fullWidth
            error={form.errors.homeAddress?.zipCode && Boolean(form.errors.homeAddress?.zipCode)}
            helperText={form.touched.homeAddress?.zipCode && form.errors.homeAddress?.zipCode}
            onChange={form.handleChange}
            value={form.values.homeAddress.zipCode}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="subtitle2">Mailing address</Typography>
        </Grid>
        <Grid item xs={3}>
          <TextField
            name="mailingAddress.lineOne"
            label="Address line 1"
            fullWidth
            error={
              form.errors.mailingAddress?.lineOne && Boolean(form.errors.mailingAddress?.lineOne)
            }
            helperText={form.touched.mailingAddress?.lineOne && form.errors.mailingAddress?.lineOne}
            onChange={form.handleChange}
            value={form.values.mailingAddress.lineOne}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            name="mailingAddress.lineTwo"
            label="Address line 2"
            fullWidth
            error={
              form.errors.mailingAddress?.lineTwo && Boolean(form.errors.mailingAddress?.lineTwo)
            }
            helperText={form.touched.mailingAddress?.lineTwo && form.errors.mailingAddress?.lineTwo}
            onChange={form.handleChange}
            value={form.values.mailingAddress.lineTwo}
          />
        </Grid>
        <Grid item xs={3}>
          <FormControl fullWidth>
            <InputLabel>State</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="mailingAddress.state"
              label="State"
              error={
                form.errors.mailingAddress?.state && Boolean(form.errors.mailingAddress?.state)
              }
              onChange={(e) => {
                form.setFieldValue('mailingAddress.city', '');
                form.handleChange(e);
              }}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.mailingAddress?.state)}
            >
              <MenuItem disabled value="">
                <span>State</span>
              </MenuItem>
              {states &&
                states.map((state) => (
                  <MenuItem key={state.id} value={state.name}>
                    {capitalize(state.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.mailingAddress?.state && form.errors.mailingAddress?.state}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={3}>
          <FormControl fullWidth>
            <InputLabel>City</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="mailingAddress.city"
              onOpen={() => handleOpenCitySelect('mailingAddress')}
              label="City"
              disabled={!form.values.mailingAddress.state}
              error={form.errors.mailingAddress?.city && Boolean(form.errors.mailingAddress?.city)}
              onChange={form.handleChange}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.mailingAddress?.city)}
            >
              <MenuItem disabled value="">
                <span>City</span>
              </MenuItem>
              {cities &&
                cities.map((city) => (
                  <MenuItem key={city.id} value={city.name}>
                    {capitalize(city.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.mailingAddress?.city && form.errors.mailingAddress?.city}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={3}>
          <TextField
            name="mailingAddress.zipCode"
            label="Zip code"
            fullWidth
            error={
              form.errors.mailingAddress?.zipCode && Boolean(form.errors.mailingAddress?.zipCode)
            }
            helperText={form.touched.mailingAddress?.zipCode && form.errors.mailingAddress?.zipCode}
            onChange={form.handleChange}
            value={form.values.mailingAddress.zipCode}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="subtitle2">Employer information</Typography>
        </Grid>
        <Grid item xs={12}>
          <FormControl component="fieldset">
            <FormLabel component="legend">Please specify if you are</FormLabel>
            <RadioGroup
              row
              value={form.values.sellerProfile.work}
              onChange={(e) => form.setFieldValue('sellerProfile.work', e.currentTarget.value)}
              name="form.sellerProfile.work"
            >
              {Object.values(WORK_POSITION_TYPES).map((type) => (
                <FormControlLabel
                  key={type}
                  value={type}
                  control={<Radio />}
                  label={capitalize(type)}
                />
              ))}
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="sellerProfile.employerName"
            label="Employer name"
            fullWidth
            error={
              form.errors.sellerProfile?.employerName &&
              Boolean(form.errors.sellerProfile?.employerName)
            }
            helperText={
              form.touched.sellerProfile?.employerName && form.errors.sellerProfile?.employerName
            }
            onChange={form.handleChange}
            value={form.values.sellerProfile?.employerName}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            name="employerAddress.lineOne"
            label="Address line 1"
            fullWidth
            error={
              form.errors.employerAddress?.lineOne && Boolean(form.errors.employerAddress?.lineOne)
            }
            helperText={
              form.touched.employerAddress?.lineOne && form.errors.employerAddress?.lineOne
            }
            onChange={form.handleChange}
            value={form.values.employerAddress.lineOne}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            name="employerAddress.lineTwo"
            label="Address line 2"
            fullWidth
            error={
              form.errors.employerAddress?.lineTwo && Boolean(form.errors.employerAddress?.lineTwo)
            }
            helperText={
              form.touched.employerAddress?.lineTwo && form.errors.employerAddress?.lineTwo
            }
            onChange={form.handleChange}
            value={form.values.employerAddress.lineTwo}
          />
        </Grid>
        <Grid item xs={3}>
          <FormControl fullWidth>
            <InputLabel>State</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="employerAddress.state"
              label="State"
              error={
                form.errors.employerAddress?.state && Boolean(form.errors.employerAddress?.state)
              }
              onChange={(e) => {
                form.setFieldValue('employerAddress.city', '');
                form.handleChange(e);
              }}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.employerAddress?.state)}
            >
              <MenuItem disabled value="">
                <span>State</span>
              </MenuItem>
              {states &&
                states.map((state) => (
                  <MenuItem key={state.id} value={state.name}>
                    {capitalize(state.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.employerAddress?.state && form.errors.employerAddress?.state}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={3}>
          <FormControl fullWidth>
            <InputLabel>City</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="employerAddress.city"
              onOpen={() => handleOpenCitySelect('employerAddress')}
              label="City"
              disabled={!form.values.employerAddress.state}
              error={
                form.errors.employerAddress?.city && Boolean(form.errors.employerAddress?.city)
              }
              onChange={form.handleChange}
              displayEmpty
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return undefined;
                }

                return selected;
              }}
              IconComponent={() => <i className="material-icons">arrow_drop_down</i>}
              value={String(form.values.employerAddress?.city)}
            >
              <MenuItem disabled value="">
                <span>City</span>
              </MenuItem>
              {cities &&
                cities.map((city) => (
                  <MenuItem key={city.id} value={city.name}>
                    {capitalize(city.name)}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText error>
              {form.touched.employerAddress?.city && form.errors.employerAddress?.city}
            </FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={3}>
          <TextField
            name="employerAddress.zipCode"
            label="Zip code"
            fullWidth
            error={
              form.errors.employerAddress?.zipCode && Boolean(form.errors.employerAddress?.zipCode)
            }
            helperText={
              form.touched.employerAddress?.zipCode && form.errors.employerAddress?.zipCode
            }
            onChange={form.handleChange}
            value={form.values.employerAddress.zipCode}
          />
        </Grid>
      </Grid>
    </form>
  );
}

PersonalInformation.displayName = 'PersonalInformation';

PersonalInformation.propTypes = {
  form: PropTypes.shape({
    errors: PropTypes.shape({
      employerAddress: PropTypes.shape({
        city: PropTypes.string,
        lineOne: PropTypes.string,
        lineTwo: PropTypes.string,
        state: PropTypes.string,
        zipCode: PropTypes.string,
      }),
      homeAddress: PropTypes.shape({
        city: PropTypes.string,
        lineOne: PropTypes.string,
        lineTwo: PropTypes.string,
        state: PropTypes.string,
        zipCode: PropTypes.string,
      }),
      mailingAddress: PropTypes.shape({
        city: PropTypes.string,
        lineOne: PropTypes.string,
        lineTwo: PropTypes.string,
        state: PropTypes.string,
        zipCode: PropTypes.string,
      }),
      primaryPhone: PropTypes.shape({
        number: PropTypes.string,
      }),
      secondaryPhone: PropTypes.shape({
        number: PropTypes.string,
      }),
      sellerProfile: PropTypes.shape({
        birthDate: PropTypes.string,
        employerName: PropTypes.string,
        title: PropTypes.string,
        estateName: PropTypes.string,
        middleName: PropTypes.string,
        socialSecurityNumber: PropTypes.string,
      }),
      user: PropTypes.shape({
        email: PropTypes.string,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
      }),
    }).isRequired,
    handleChange: PropTypes.func,
    handleSubmit: PropTypes.func,
    setFieldValue: PropTypes.func,
    touched: PropTypes.shape({
      employerAddress: PropTypes.shape({
        city: PropTypes.bool,
        lineOne: PropTypes.bool,
        lineTwo: PropTypes.bool,
        state: PropTypes.bool,
        zipCode: PropTypes.bool,
      }),
      homeAddress: PropTypes.shape({
        city: PropTypes.bool,
        lineOne: PropTypes.bool,
        lineTwo: PropTypes.bool,
        state: PropTypes.bool,
        zipCode: PropTypes.bool,
      }),
      mailingAddress: PropTypes.shape({
        city: PropTypes.bool,
        lineOne: PropTypes.bool,
        lineTwo: PropTypes.bool,
        state: PropTypes.bool,
        zipCode: PropTypes.bool,
      }),
      primaryPhone: PropTypes.shape({
        number: PropTypes.bool,
      }),
      secondaryPhone: PropTypes.shape({
        number: PropTypes.bool,
      }),
      sellerProfile: PropTypes.shape({
        birthDate: PropTypes.bool,
        employerName: PropTypes.bool,
        estateName: PropTypes.bool,
        title: PropTypes.bool,
        middleName: PropTypes.bool,
        socialSecurityNumber: PropTypes.bool,
      }),
      user: PropTypes.shape({
        email: PropTypes.bool,
        firstName: PropTypes.bool,
        lastName: PropTypes.bool,
      }),
    }),
    values: PropTypes.shape({
      employerAddress: PropTypes.shape({
        city: PropTypes.string,
        lineOne: PropTypes.string,
        lineTwo: PropTypes.string,
        state: PropTypes.string,
        zipCode: PropTypes.string,
      }),
      homeAddress: PropTypes.shape({
        city: PropTypes.string,
        lineOne: PropTypes.string,
        lineTwo: PropTypes.string,
        state: PropTypes.string,
        zipCode: PropTypes.string,
      }),
      mailingAddress: PropTypes.shape({
        city: PropTypes.string,
        lineOne: PropTypes.string,
        lineTwo: PropTypes.string,
        state: PropTypes.string,
        zipCode: PropTypes.string,
      }),
      primaryPhone: PropTypes.shape({
        number: PropTypes.string,
      }),
      secondaryPhone: PropTypes.shape({
        number: PropTypes.string,
      }),
      sellerProfile: PropTypes.shape({
        birthDate: PropTypes.string,
        employerName: PropTypes.string,
        estateName: PropTypes.string,
        listingOwnership: PropTypes.string,
        middleName: PropTypes.string,
        ownership: PropTypes.string,
        title: PropTypes.string,
        socialSecurityNumber: PropTypes.string,
        work: PropTypes.string,
      }),
      user: PropTypes.shape({
        email: PropTypes.string,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
      }),
    }).isRequired,
  }).isRequired,
};

export default PersonalInformation;
