import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import PropTypes from 'prop-types';
import LoadingButton from '@mui/lab/LoadingButton';
import { useSelector } from 'react-redux';
import { checkIfIsLoading } from 'features/common/slices/api/index';

function SignUpWizard(props) {
  const isLoading = useSelector(checkIfIsLoading);
  const { steps, currentStep, handleBack, handleNext } = props;
  return (
    <Container>
      <Typography variant="h2" gutterBottom sx={{ textAlign: 'center' }}>
        Registration
      </Typography>
      <Stepper activeStep={currentStep} alternativeLabel>
        {steps.map((step) => (
          <Step key={step.label}>
            <StepLabel>{step.label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <Box sx={{ mt: 8 }}>{steps[currentStep].pageComponent()}</Box>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'space-between',
          mt: 8,
          pb: 8,
        }}
      >
        <Button
          color="secondary"
          variant="outlined"
          disabled={currentStep === 0}
          onClick={handleBack}
        >
          Back
        </Button>
        <LoadingButton
          color="secondary"
          loading={isLoading}
          variant="outlined"
          onClick={handleNext}
        >
          {currentStep === steps.length - 1 ? 'Finish' : 'Next'}
        </LoadingButton>
      </Box>
    </Container>
  );
}

SignUpWizard.displayName = 'signUpWizard';

SignUpWizard.propTypes = {
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      pageComponent: PropTypes.func.isRequired,
    })
  ).isRequired,
  currentStep: PropTypes.number.isRequired,
  handleBack: PropTypes.func.isRequired,
  handleNext: PropTypes.func.isRequired,
};

export default SignUpWizard;
