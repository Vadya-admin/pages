import loadable from '@loadable/component';
import Loading from 'features/common/components/Loading';

const SignUpPage = loadable(() => import(/* webpackChunkName: "register-page" */ './page'), {
  resolveComponent: ({ SignUpPage: Page }) => Page,
  fallback: <Loading />,
});

export default SignUpPage;
