import { Provider } from 'react-redux';

import store from 'app/store';

const withRedux = (Component) =>
  function ({ ...props }) {
    return (
      <Provider store={store}>
        <Component {...props} />
      </Provider>
    );
  };

export default withRedux;
