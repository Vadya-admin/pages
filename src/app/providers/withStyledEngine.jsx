import { StyledEngineProvider } from '@mui/material/styles';
import AdapterDateFns from '@mui/lab/AdapterDateFns';

import LocalizationProvider from '@mui/lab/LocalizationProvider';

const withStyledEngine = (Component) =>
  function (props) {
    return (
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <StyledEngineProvider injectFirst>
          <Component {...props} />
        </StyledEngineProvider>
      </LocalizationProvider>
    );
  };

export default withStyledEngine;
