const paths = {
  home: () => '/',
  logout: () => '/logout',
  notFound: () => '/404',
  seller: () => '/seller',
  buyer: () => '/buyer',
  admin: () => '/admin',
  signIn: () => '/sign-in',
  signUp: () => '/sign-up',
  forgotPassword: () => '/forgot-password',
  /** SELLER ROUTES */
  sellerProfile: () => `${paths.seller()}/profile`,
  dashboard: () => `${paths.seller()}/dashboard`,
  currentListings: () => `${paths.seller()}/current-listings`,

  sellerListing: () => `${paths.seller()}/listing`,
  sellerListingInfo: () => `${paths.sellerListing()}/:listingId`,

  editListingBase: () => `${paths.seller()}/edit-listing`,
  editListing: () => `${paths.editListingBase()}/:listingId`,

  addListing: () => `${paths.seller()}/add-listing`,
  
  propostions: () => `${paths.seller()}/propostions`,
  portfolio: () => `${paths.seller()}/portfolio`,
  activeContractsSeller: () => `${paths.seller()}/active-contracts`,
  currentBids: () => `${paths.seller()}/current-bids`,
  /** BUYER ROUTES */
  buyerListing: () => `${paths.buyer()}/listing`,
  buyerListingInfo: () => `${paths.buyerListing()}/:listingId`,

  buyerProfile: () => `${paths.buyer()}/profile`,
  dashboardBuyer: () => `${paths.buyer()}/dashboard`,
  overview: () => `${paths.buyer()}/overview`,
  searchListings: () => `${paths.buyer()}/search-listings`,
  suggestedListings: () => `${paths.buyer()}/suggested-listings`,
  watchList: () => `${paths.buyer()}/watch-list`,
  preferences: () => `${paths.buyer()}/preferences`,
  contractsOverview: () => `${paths.buyer()}/contracts-overview`,
  activeContractsBuyer: () => `${paths.buyer()}/active-contracts`,
  expiredContracts: () => `${paths.buyer()}/expired-contracts`,
  portfolioHistory: () => `${paths.buyer()}/portfolio-history`,
  bidHistory: () => `${paths.buyer()}/bid-history`,
  clientList: () => `${paths.buyer()}/client-list`,

   /** BUYER ROUTES */
   users: () => `${paths.admin()}/users`,
   listings: () => `${paths.admin()}/listings`,
};



export default paths;
