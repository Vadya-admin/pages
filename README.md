## Scripts 🏷️

### Install

```bash
npm install
```

### Start

```bash
npm start
```

or

```bash
npm run dev
```

### build

```bash
npm run build
```
