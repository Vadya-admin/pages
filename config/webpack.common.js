const { resolve, join } = require('path');
const { DefinePlugin } = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin');

const getClientEnvironment = require('./env');

const paths = require('./paths');

const DIST = resolve(__dirname, '..', 'dist');

const ENTRY_PATH = resolve(__dirname, '..', 'src/app/index.jsx');
const ENTRY_HTML_FILE = resolve(__dirname, '..', 'public/index.html');

const isDevelopment = process.env.NODE_ENV === 'development';
const PUBLIC_PATH = '/';
const env = getClientEnvironment(PUBLIC_PATH);

module.exports = {
  target: 'web',
  entry: {
    index: ['react-hot-loader/patch', ENTRY_PATH],
  },
  output: {
    path: DIST,
    filename: isDevelopment ? '[name].bundle.js' : '[contenthash:8].bundle.js',
    chunkFilename: isDevelopment ? 'chunk-[name].js' : 'chunk-[name].[contenthash].js',
    assetModuleFilename: isDevelopment ? 'assets/[name][ext][query]' : 'assets/[hash][ext][query]',
    publicPath: PUBLIC_PATH,
    pathinfo: true,
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx', '.css'],
    alias: {
      'app': paths.app,
      'pages': paths.pages,
      'features': paths.features,
      'shared': paths.shared,
      '@fuse': paths.fuse,
      'assets': paths.assets,
      'styles': paths.styles,
      'src': paths.src,
      ...(isDevelopment ? { 'react-dom': '@hot-loader/react-dom' } : undefined),
    },
    modules: [join(__dirname, 'src'), 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.(js)x?$/i,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|png|svg|jpg|jpeg|)(\?v=\d+\.\d+\.\d+)?$/,
        type: 'asset/resource',
      },
      // {
      //   test: /\.(jpe?g|gif|png|webp|avif)$/,
      //   use: {
      //     loader: 'url-loader',
      //   },
      // },
      // {
      //   test: /.(png|jpe?g|gif|webp|avif)$/i,
      //   use: [{ loader: 'file-loader' }],
      // },
      {
        test: /\.module\.css$/,
        use: [
          !isDevelopment ? MiniCssExtractPlugin.loader : 'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: {
                localIdentName: '[name]-[local]_[hash:base64:5]',
              },
            },
          },
          'postcss-loader',
        ],
      },
      {
        test: /(?<!\.module)\.css$/,
        use: [
          !isDevelopment ? MiniCssExtractPlugin.loader : 'style-loader',
          'css-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'babel-loader',
          },
          {
            loader: 'url-loader',
          },
          {
            loader: '@svgr/webpack',
            options: {
              babel: false,
              icon: true,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new DefinePlugin(env.stringified),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      title: 'Alphamize',
      chunks: ['vendor', 'index'],
      chunksSortMode: 'manual',
      template: ENTRY_HTML_FILE,
      favicon: resolve(__dirname, '..', 'public/favicon.ico'),
    }),
    new InterpolateHtmlPlugin(HtmlWebpackPlugin, env.raw),
  ],
};
