const config = {
  hot: true,
  compress: true,
  historyApiFallback: {
    disableDotRule: true,
  },
  static: {
    publicPath: '/',
  },
  port: 8090,
  headers: {
    'Access-Control-Allow-Origin': '*',
  },
};

module.exports = config;
